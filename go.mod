module gitlab.com/gartnera/caddy-gitlab-login

go 1.12

replace github.com/tarent/loginsrv => github.com/gartnera/loginsrv v1.3.1-0.20190927164936-66990f218258

replace github.com/h2non/gock => gopkg.in/h2non/gock.v1 v1.0.14

replace github.com/labbsr0x/goh => github.com/labbsr0x/goh v1.0.1

require (
	github.com/BTBurke/caddy-jwt v3.7.1+incompatible
	github.com/abiosoft/caddy-git v0.0.0-20190703061829-f8cc2f20c9e7
	github.com/caddyserver/caddy v1.0.4
	github.com/caddyserver/dnsproviders v0.4.0
	github.com/captncraig/caddy-realip v0.0.0-20190710144553-6df827e22ab8
	github.com/captncraig/cors v0.0.0-20190703115713-e80254a89df1
	github.com/epicagency/caddy-expires v1.1.1
	github.com/google/uuid v1.1.1
	github.com/klauspost/cpuid v1.2.1
	github.com/mholt/certmagic v0.8.3
	github.com/nicolasazrak/caddy-cache v0.3.4
	github.com/pquerna/cachecontrol v0.0.0-20180517163645-1555304b9b35 // indirect
	github.com/tarent/loginsrv v0.0.0-00010101000000-000000000000
	gopkg.in/natefinch/lumberjack.v2 v2.0.0
)
